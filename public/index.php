<?php
session_start();
include('../controler/func.php');
include('../controler/conf.php');
$page = key($_GET) ?? 'accueil';
$page_secure = array('base','accueil','devis','form','formphoto','cons','consp','pdf','admin');

if(in_array($page, $page_secure,true)){
  
    include('../models/'.$page.'.php');
    include('../models/header.php');
    include('../models/footer.php');
    $header = headerAction($page);
    $footer = footerAction($page);
    $array  = array( 'header' => $header,'footer'=> $footer);
    $content = Action($conf);
    $array['content'] = $content;
	echo template("../view/base.html.php", $array);
} 
else {
     header("Status", true, 403);
} 

?>
