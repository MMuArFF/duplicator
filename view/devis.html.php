<?php
if(isset($_POST['nb_f']))
{ 
  $machine = $prix['devis']['machine']?>
   
  <div class="alert alert-info">
   <strong>Info!</strong>
    Vous désirez utiliser <?= $_POST['nb_f'] ?> feuilles à  <?= $prix['papier'] ?> euros soit <?= $prix['devis']['f']?> euros. <?php if($machine !="photocop"){
    ?>  Chaque master coute<?= $prix[$machine]['master']['unite']  ?> soit pour <?= $_POST['nb_m']?> master <?= $prix['devis']['m']?> euros <?php } ?>
  Chaque passage coute <?=  $prix[$machine]['encre']['unite']?> soit pour <?= $prix['devis']['nb_p']?> passages  <?= $prix['devis']['p']?> euros<br />Soit un total de <font color="red"><strong><?= $prix['devis']['t']?> euros</Strong></font><a href="index.php"> Accueil</a>
  </div>
  <div class="alert alert-danger"><center>
  <strong>Attention!</strong> Ni les locaux, ni l'entretien des  machines ne sont comptés dans ce chiffre. Merci de donner un peu plus que demandé, si possible
  </center>
  </div>
<?php } ?>
<form class="form-horizontal" action="" method="post">
<fieldset>
<legend>Devis</legend>
<div class="form-group">
  <label class="col-md-4 control-label" for="machine">Quelle machine ?</label>  
  <div class="col-md-4">
  
    <select id="machine1" name="machine" onclick="test()" class="form-control">
      <option value="pA4">photocopilleuse A4</option>
      <option value="pA3">photocopilleuse A3</option>
      <option value="A4">Duplicopieur A4</option>
      <option value="A3">Duplicopieur A3</option>
    </select>
   

  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="nb_f">Nombre de tirages</label>  
  <div class="col-md-4">
  <input id="nb_f" name="nb_f"  class="form-control input-md" required type="number">
  <span class="help-block">Ecrivez ici le nombre de feuilles que vous désirez utiliser</span>  
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="rv">Recto/verso</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="rv-0">
      <input name="rv" id="rv-0" value="oui" type="radio" checked>
      oui
    </label>
     <label for="rv-1">
      <input name="rv" id="rv-1" value="non" type="radio">
      non
    </label>
	</div>
  </div>
</div>
    <hr><div class="col-md-4"></div>
      <div  id="reveal" class="form-group col-md-4" style="display: none" >
 
     
    
  <label class="control-label" margin="-50px" for="nb_m">Nombre de master</label> 

  <input name="nb_m" id="nb_m" class="col-md-4 form-control input-md"  type="number">

</div> 

   
   <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">Résultat. tininin !!</button></div>
        </div>
      </div>
    </div>
   </fieldset></form>

