<table id="example" class="table table-striped " cellspacing="0" width="100%">
  <thead>
    <th style="width:10%;">changement</th><th style="width:13%;">nb actuel</th><th style="width:13%;">Moyenne</th><th style="width:10%;">temps moyen<th style="width:10%;">Prochain</th><th style="width:10%;">dernier</th><th style="width:10%;">prix calculé (eur/unité)</th><th style="width:7%;">prix utilisé</th><th style="width:7%;">prix à l'achat</th><th>edit</th></thead>
      <tbody>
        <?php 
         // print_array($machines);
          foreach ($machines as $key => $val) 
          {
            if(count($val)> 1)
            {
              foreach ($val as $type) 
              {
                 if($type == 'encre'){ $mot ="passage"; $nb = 'nb_p';}
                 if($type == 'master'){ $mot ="master"; $nb = 'nb_m';}
                
                  ?>
                    <tr class="<?= $cons[$key][$type]['class'] ?>">
                      <td><?=$type?> <?=$key?></td>
                      <td><?= ceil($cons[$key][$type]['nb_actuel'])?> <?=$mot?></td>
                      <td><?= ceil($cons[$key][$type]['moyenne_totale'][$nb])?> <?=$mot?></td>
                      <td><?= ceil($cons[$key][$type]['moyenne_totale']['temps']/86400) ?> jours</td>
                      <td><?= ceil($cons[$key][$type]['temps_jusqua']/86400)?>jours</td>
                      <td><?= ceil($cons[$key][$type]['temps_depuis']/86400)?> jours</td>
                      <td style="color:<?= $cons[$key][$type]['color'];?>"><strong><?=  round($cons[$key][$type]['prix_calcule'],4)?></strong> euros</td>
                      <td><form method="post"><div class="form-group"><input   class="form-control input-sm" name="prix_unite" value ="<?= round($prix[$key][$type]['unite'],4)?>" /> </div></td>
                      <td><div  class="form-group"><input   class="form-control input-sm" name="prix_pack" value ="<?= round($prix[$key][$type]['pack'],4)?>" /> </div></td>
                      <td><input type="hidden" value="<?=$key?>" name="machine" /><input type="hidden" value="<?=$type?>" name="type" /><button id="singlebutton" type="submit" class="btn btn-warning">change</button></form></td>
                    </tr>
                  <?php 
              }
            }
            else 
            { 
              $mot = "passages";?>
              <tr class="<?= $cons[$key][$key]['class'] ?>">
                <td><?=$key?></td>
                <td><?= ceil($cons[$key][$key]['nb_actuel'])?> </td>
                <td><?= ceil($cons[$key][$key]['moyenne_total']['nb_p'])?> <?=$mot?></td>
                <td><?= ceil($cons[$key][$key]['moyenne_total']['temps']/86400) ?> jours</td>
                <td><?= ceil($cons[$key][$key]['temps_jusqua']/86400)?>jours</td>
                <td><?= ceil($cons[$key][$key]['temps_depuis']/86400)?> jours</td>
                <td style="color:'.$colorpa3.';"><strong><?=  round($cons[$key][$key]['prix_calcule'],4)?></strong> euros</td>
                <td><form method="post"><div class="form-group"><input   class="form-control input-sm" name="prix_unite" value ="<?= round($prix[$key]['encre']['unite'],4)?>" /> </div></td>
                <td><div  class="form-group"><input type="hidden" value="encre" name="type" /><input type="hidden" value="photocop" name="machine" />  <input   class="form-control input-sm" name="prix_pack" value ="<?= round($prix[$key]['encre']['pack'],4)?>" /> </div></td>
                <td><button id="singlebutton" name="singlebutton" type="submit" class="btn btn-warning">change</button></form></td>
              </tr>

            <?php }
          } ?>


                      </tbody></table><hr>
                      
                      <div> <script type="text/javascript">
function calc(A3)
{  
  A4 = parseFloat(A3)/2;
  document.getElementById('A4').value=A4;
  return false;
}
</script>
                      <form class="form-horizontal" method="post">
<fieldset><?php 
for($i=0; $i <2;$i++){
  ($i == 0)? $tail = 'A3':$tail ='A4';
  ?>
<div class="form-group">
  <label class="col-md-5 control-label" for="textinput">Prix Feuilles <?= $tail ?></label>  
  <div class="col-md-2">
    <input id="<?= $tail ?>" <?= ($i == 0)? 'onChange="calc(this.value)"':'' ?> step ="0.001" name="papier_<?= $tail ?>" value="<?=$prix['papier'][$tail]?>" <?= ($i == 1)? "disabled" :""?> class="form-control input-md" type="number">
  </div>
</div>
<?php } ?>
<div class="form-group">
  <label class="col-md-5 control-label" for="singlebutton"></label>
  <div class="col-md-2">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">sauvegarder</button>
  </div>
</div>
</fieldset>
</form>