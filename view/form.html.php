
<?php

if(isset($_POST['machine']) && isset($_POST['Contact']) && isset($_POST['ok']))
{
      ?>
      <div class="alert alert-success">
  <strong>Succes!</strong> Ton fichage est fiché !
  </div><div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><a href="index.php"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">Accueil</button></a></div>
        </div>
      </div>
    </div
        
   <?php 
}else{
?>
      <form class="form-horizontal" action="" method="post">
<fieldset>
    <div class="form-group">
  <label class="col-md-4 control-label" for="machine">Tout d'abord choisis ta la machine</label>
  <div class="col-md-4">
    <select id="machine" name="machine" class="form-control" onchange="this.form.submit()">>
      <option value=""></option>
      <option value="A4" <?= ($machine == "A4") ? "selected" : "";?> >Duplicopieur A4</option>
      <option value="A3" <?= ($machine == "A3")? "selected" : "";?>>Duplicopieur A3</option>
      </select>
   
  </div>
    </div></fieldset></form>

<form class="form-horizontal" action="#after" method="post">
    <div class="form-group">
        <input type="hidden" value ="<?= $machine ?>" name="machine"/>
  <label class="col-md-4 control-label" for="Contact">Contact</label>  
  <div class="col-md-4">
  <input id="Contact" name="Contact" <?= !empty($contact) ? 'value="'.$contact.'"' : 'placeholder="me@example.com"';?> class="form-control input-md" required type="text" <?= empty($machine) ? "disabled" :"";?>>
      <?= empty($machine) ? '<span class="help-block">Choissisez d\'abord la machine !</span>  ' : '<span class="help-block">Un mail, pseudo explicite</span>  ';?>
   
  </div>
</div>
<!-- Multiple Checkboxes -->
<div class="form-group">
  <label class="col-md-4 control-label" for="rv">Recto/verso</label>
  <div class="col-md-4">
  <div class="checkbox">
    <label for="rv-0">
      <input name="rv" id="rv-0" value="oui" type="checkbox" <?php if(isset($_POST['rv'])){echo "checked";} ?>>
      oui
    </label>
	</div>
  </div>
</div><?php if($machine =="A4"){?>
<div class="form-group">
  <label class="col-md-4 control-label" for="rv">2eme couleur ? ( feuilles déjà payées)</label>
  <div class="col-md-4">
  <div class="checkbox">
    <label for="rv-1">
      <input name="couleur" id="rv-1" value="oui" type="checkbox" <?php if(isset($_POST['couleur'])){echo "checked";} ?>>
      oui
    </label>
  </div>
  </div>
</div>
<?php }?>
<!-- Select Basic -->

<img src="img/lagaf.png" class="center-block img-responsive img-thumbnail">
<hr>
<div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
  Signaler un changement d'encre et/ou de master <a href="cons.php" target="_blank">Dupli</a> 
</div>
   <div class="col-md-2"></div>
     <div class="col-md-4">

         <legend>Avant</legend>
    <div class="form-group">
  <label class="col-md-4 control-label" for="master_av">Nombre de Masters AVANT</label>  
  <div class="col-md-4">
  <input id="A3" name="master_av" <?= !empty($machine) ? 'value="'.$master_av.'"' : 'value="" disabled'  ?> class="form-control input-md" required type="text" >
  </div></div><br /><br /><br /><br />
         <div class="form-group">
  <label class="col-md-4 control-label" for="passage_av">Nombre de Passages AVANT</label>  
  <div class="col-md-4">
  <input id="A4" name="passage_av"  <?= !empty($machine) ? 'value="'.$passage_av.'"' : 'value="" disabled'  ?> class="form-control input-md" required type="text">
  </div>
</div>
</div>
 <div class="col-md-4">
     <legend>Après</legend>
    <div class="form-group">
  <label class="col-md-4 control-label" for="master_av">Nombre de Masters APRES</label>  
  <div class="col-md-4">
  <input id="" name="master_ap" <?= !empty($master_ap) ? 'value="'.$master_ap.'"' : 'value=""'  ?> class="form-control input-md" required type="text">
  <span class="help-block"></span>  
  </div>
</div><br /><br /><br /><br />
         <div class="form-group">
  <label class="col-md-4 control-label" for="passage_av">Nombre de Passages APRES</label>  
  <div class="col-md-4">
  <input id="passage_ap" name="passage_ap" <?= !empty($passage_ap) ? 'value="'.$passage_ap.'"' : 'value=""' ?> class="form-control input-md" required type="text">
  <span class="help-block"></span>  
  </div>
</div>
</div>
      
   <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><button id="singlebutton" name="singlebutton"  class="btn btn-success btn-block">Combien ca me coute ?</button></div>
        </div>
      </div>
    </div></form>

   <?php if(isset($_POST['machine']) && isset($_POST['Contact']))
  //print_array($prix);
  { ?>
    <div id="after" class="alert alert-info">
    <strong>Info!</strong>Vous désirez utiliser <?= $nb_f ?> feuilles à  <?= $prix['papier'] ?> euros soit <?= $prix['devis']['f']?> euros. Chaque master coute<?= $prix[$machine]['master']['unite']  ?> soit pour <?= $nb_m?> master <?= $prix['devis']['m']?> euros. Chaque passage coute <?= $prix[$machine]['encre']['unite'] ?> soit pour <?= $nb_p?> passages  <?= $prix['devis']['p']?> euros<br />Soit un total de <font color="red"><strong><?= $prix['devis']['t']?> euros</Strong></font><a href="index.php"> Accueil</a>
    </div>
    
    <div class="alert alert-danger"><center>
    <strong>Attention!</strong> Ni les locaux, ni l\'entretien des  machines ne sont comptés dans ce chiffre. Merci de donner un peu plus que demandé, si possible
    </center>
   </div>
  <?php } if(isset($phrase)){ echo '<img src="img/picsou.jpg" class="center-block img-responsive img-thumbnail"><center><div id="after" class="alert alert-info">
  <strong>Info!</strong> '.$phrase.'
</div></center><div class="alert alert-danger"><center>
  <strong>Attention!</strong> '.$phrase2.'
</center></div>';} ?>
<form class="form-horizontal" action="" method="post">
<fieldset>
<legend>Enregistrement du tirage</legend>
 <div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
  Signaler un changement d'encre et/ou de master <a href="cons.php" target="_blank">Dupli</a> 
</div>
    <input type="hidden" value ="<?php echo $machine ;?>" name="machine"/>
    <input type="hidden" value ="<?php echo $_POST['Contact'] ;?>" name="Contact"/>
    <input type="hidden" value ="<?php echo $rv ;?>" name="rv"/>
    <input type="hidden" value ="<?php echo $_POST['master_av'] ;?>" name="master_av"/>
    <input type="hidden" value ="<?php echo $_POST['master_ap'] ;?>" name="master_ap"/>
    <input type="hidden" value ="<?php echo $_POST['passage_av'] ;?>" name="passage_av"/>
    <input type="hidden" value ="<?php echo $_POST['passage_ap'] ;?>" name="passage_ap"/>
    <input type="hidden" value ="ok" name="ok"/>
    <input type="hidden" value ="<?php echo $prix_t ?>" name="prix"/>
      <div class="form-group">
  <label class="col-md-4 control-label" for="paye">As tu payé</label>
  <div class="col-md-4">
  <div class="radio">
    
      <input name="paye" id="payeoui" value="oui" type="radio" required >
      <label for="payeoui">oui
    </label>
      <div class="reveal-if-active">
 
    <div class="form-group">
  <label for="cb1">si oui, combien ?</label>
  
  <input id="cb1" name="cb" value="" data-require-pair="#payeoui" class="form-control input-md require-if-active" type="text" >
        </div>  
</div>
      <input  name="paye" id="paye" value="non" type="radio" required>
       
      <label for="paye">
      non
    </label>
	</div>
     
  </div>
</div>
 
        
         
    <div class="form-group">
  <label class="col-md-4 control-label" for="master_av">Un petit mot, une réclamation, un encouragement, une finfo?</label>  
  <div class="col-md-4">
  <textarea id="mot" name="mot" value="" class="form-control input-md" > </textarea>
        </div></div>
     <hr><div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">Enregistrer !</button></div>
        </div>
      </div>
    </div></fieldset></form>

<?php } ?>