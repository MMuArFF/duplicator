	<?php
if(!isset($_SESSION['user'])&& !isset($_POST['password']))
{
		?>
      <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Le sésame de l'administration</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
            <form role="form" method="post">
              <div class="form-group">
                <div class="input-group">
                  <input type="password" class="form-control" name="password" placeholder="Sésame">
                  <span class="input-group-btn">
                      <button class="btn btn-success" type="submit" placeholder="Ouvre toi!">Ouvre toi!</button>
                  </span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
   </body>

    </html>
   <?php 
}
if(isset($_SESSION['user']))
{
    if(isset($_GET['news']))
    {
      include('admin.news.html.php');
    }
    if(isset($_GET['edit']))
    {
      include('admin.edit.html.php');
    }
    if(isset($_GET['prix']))
    {
      include('admin.prix.html.php');
    }
    elseif(isset($_GET['tirages']))
    { 
      include('admin.tirage.html.php');
    }
    elseif(isset($_GET['mots']))
    { 
      include('admin.mot.html.php');
    }        
    elseif(isset($_POST['delmail']))
    {
      ?>
      <div class="alert alert-success">
        <strong>Succes!</strong> bien supprimés ! 
      </div>

      <?php 
    } 
    else
    {
       ?>
        <div class="section">
            <div class="container">
              <div class="row">
                  <h1 align = "center"><a href="?admin&tirages" >Derniers Tirages </a></h1><hr>
                  <h1 align = "center"><a href="?admin&prix" >Gestion des prix </a></h1><hr>
                  <h1 align = "center"><a href="?admin&news" >Gestion des news </a></h1><hr>
                  <h1 align = "center"><a href="?admin&mots" >Mots doux</a></h1><hr>
       <?php
     
      if(!empty($emails))
      { ;
        ?>
        <div  class="col-md-4"></div><div align="center" class="col-md-4">Voici des email à entrer dans la liste de difusion :<br />
                  <ul class"=list-group">

                        <?php
                         foreach ($emails as $email => $value) {?>
                             <li  class="list-group-item"><?= $value ?></li>
                          
                        <?php } 
                       ?>
                    </ul><form method="POST">
                  <button name="delmail" class="btn btn-danger">Supprimer</button></form></div>

        </div></div></div>
        <?php 
      }
   }
}  
  ?>
