<?php
if(!isset($_POST['Contact'])) 
  {?>
           <form class="form-horizontal" action="" method="post">
        <fieldset>
        <legend>Enregistrement du tirage</legend>
      <div class="form-group">
        <label class="col-md-4 control-label" for="Contact">Contact</label>  
        <div class="col-md-4">
        <input id="Contact" name="Contact" placeholder="me@example.com" class="form-control input-md" required type="text">
        <span class="help-block">Un mail, pseudo explicite</span>  
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="Contact">Nombre de feuilles</label>  
        <div class="col-md-4">
        <input id="Contact" name="nb_f"  class="form-control input-md" required type="text">
        <span class="help-block"></span>  
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="radios">Taille</label>
        <div class="col-md-4"> 
          <label class="radio-inline" for="radios-0">
            <input name="radios" id="radios-0" value="A4" checked="checked" type="radio">
            A4
          </label> 
          <label class="radio-inline" for="radios-1">
            <input name="radios" id="radios-1" value="A3" type="radio">
            A3
          </label>
        </div>
      </div>
      <div class="form-group">
        <label class="col-md-4 control-label" for="rv">Recto/verso</label>
        <div class="col-md-4">
        <div class="checkbox">
          <label for="rv-0">
            <input name="rv" id="rv-0" value="oui" type="checkbox">
            oui
          </label>
      	</div>
        </div>
      </div>
         <div class="section">
            <div class="container">
              <div class="row">
                <div class="col-md-12"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">suivant</button></div>
              </div>
            </div>
          </div>
         </fieldset></form>
<?php }
if(isset($_POST['Contact']) && !isset($_POST['ok']))
{?>
       <div class="alert alert-info">
    <strong>Info!</strong> Vous utilisez <?= $nb_f ?> feuilles chaque feuille coute <?= $prix['papier'][$papier]?> soit <?= $prix['prix_papier'] ?> euros. Chaque passage coute <?= $prix['unitaire'] ?> soit <?= $prix['prix_passage'] ?> euro pour <?= $nb_p?> passage . Soit un total de <font color="red"><strong><?= $prix['total']?> euros</Strong></font><a href="index.php"> Accueil</a>
    </div>
    <div class="alert alert-danger"><center>
    <strong>Attention!</strong> Ni les locaux, ni l'entretien des  machines ne sont comptés dans ce chiffre. Merci de donner un peu plus que demandé, si possible
    </center>
   </div>
    <hr>
        <form class="form-horizontal" action="" method="post">
<fieldset>
<legend>Enregistrement du tirage</legend>
    <input type="hidden" value ="<?= $papier ?>" name="machine"/>
    <input type="hidden" value ="<?= $_POST['Contact'] ;?>" name="Contact"/>
    <input type="hidden" value ="<?= $rv ;?>" name="rv"/>
    <input type="hidden" value ="<?= $nb_f;?>" name="nb_f"/>
    <input type="hidden" value ="<?= $prix['total'] ?>" name="prix"/>
     <input type="hidden" value ="ok" name="ok"/>
     <div class="alert alert-danger" role="alert">
  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  <span class="sr-only">Error:</span>
  Signaler un changement d'encre et/ou de master <a href="cons.php" target="_blank">Dupli</a> | <a href="consp.php" target="_blank">Photocop</a>
</div>
    <div class="form-group">
  <label class="col-md-4 control-label" for="paye">As tu payé</label>
  <div class="col-md-4">
  <div class="checkbox">
    <label for="paye">
      <input name="paye" id="paye" value="oui" type="checkbox">
      oui
    </label>
	</div>
  </div>
</div>
 
         
    <div class="form-group">
  <label class="col-md-4 control-label" for="master_av">si oui, combien ?</label>  
  <div class="col-md-4">
  <input id="cb" name="cb" value="" class="form-control input-md"  type="text">
        </div></div>
         
    <div class="form-group">
  <label class="col-md-4 control-label" for="master_av">Un petit mot, une réclamation, un encouragement, une finfo?</label>  
  <div class="col-md-4">
  <textarea id="mot" name="mot" value="" class="form-control input-md" > </textarea>
        </div></div>
     <hr><div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">Enregistrer !</button></div>
        </div>
      </div>
    </div>
   </fieldset></form>
    
    
    <?php
   
    }
         if(isset($_POST['machine']) && isset($_POST['Contact']) && isset($_POST['ok']))
    {
         ?>
       <div class="alert alert-success">
  <strong>Succes!</strong> Ton fichage est fiché !
</div><div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><a href="index.php"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">Accueil</button></a></div>
        </div>
      </div>
    </div>
      <?php  
    }
    ?>