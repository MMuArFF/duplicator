
            <h1>Bienvenue duplicateur-euse</h1>
            <hr>
            <p>Si tu as 5 minutes, tu peux liere le<a href="#collectif"> fonctionnement du collectif</a>, connaître
              les <a href="#info">dernieres infos</a>, Ou bien t'inscrire à la <a href="#diffusion">liste de diffusion</a> ( 2-3 mails
                par trimestre).&nbsp;Où encore as-tu du temps à perdre en regardant les <a href="#stats">stats</a>. Mais peut-être désire tu juste tirer tes tracts !</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-4"><a href="?devis" target="_blank" style="text-decoration:none">
            <img src="img/k10381840.jpg" class="center-block img-responsive img-thumbnail">
            <h2>Devis</h2>
            <p>Tu désires connaître le prix de ton tirage avant de te lancer, clique
              ici !</p></a>
          </div>
          <div class="col-md-4"><a href="?formphoto" target="_blank" style="text-decoration:none">
            <img src="img/photocop.jpg"
            class="img-responsive">
            <h2>Photocopilleuse</h2>
            <p>Avant d'utiliser le dupli, ou juste pour imprimer la derniere brochure,
                en tout cas pas pour tirer tes tracts en grands exemplaires!</p></a>
          </div>
          <div class="col-md-4"><a href="?form" target="_blank" style="text-decoration:none">
            <img src="img/dupli.jpg" class="img-responsive">
            <h2>Duplicat0r</h2>
            <p>Enregistre ton tirage et permet nous de mieux gérer les consommables.</p></a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <img src="img/{130ED785-3FAA-4264-8AF9-A95CD3CF1B94}_extra.gif"
                    class="img-responsive">
                  </div>
                  <div class="col-md-6" id="info">
                    <h1>Dernieres nouvelles.</h1>
                      <?php 
                      for ($i = 0;$i < count($news);$i++) 
                      {?>
                        <h3><?= $news[$i]['titre'] ?></h3>
                            <div align="right"><strong><?= $news[$i]['time'] ?></strong></div>
                            <div><?= html_entity_decode($news[$i]['news']) ?></div>';
                        <?php 
                        
                      }  ?>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6" id="collectif">
            <h1 contenteditable="true">Presentation du collectif.</h1>
            <h3>Un peu d'histoire</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
              ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis
              dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies
              nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In
              enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum
              felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
              elementum semper nisi.</p>
          </div>
          <div class="col-md-6">
            <img class="center-block img-responsive" style="opacity: 0.5;" src="img/reunion-2_4915317.jpeg">
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12" id="diffusion">
            <h1 class="text-center">S'inscrire à la liste de Diffusion</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
              <?php if(isset($_POST['email'])){ echo $email;}else {?>
            <form role="form" action="#diffusion"method="post">
                
              <div class="form-group">
                <div class="input-group">
                    
                  <input type="email" name = "email" class="form-control" placeholder="email">
                  <span class="input-group-btn">
                    <input class="btn btn-success" type="submit">
                  </span>
                </div>
              </div>
            </form><?php } ?>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div id="stats" class="row">
            <h1 >Stats</h1>
            <div class="alert alert-info">
            Depuis le debut de l'aventure dupli en 2011, nous avons tiré un total de <strong><?= ceil($stats['nb_f']) ?> pages</strong> en plus de <strong><?= $stats['nb_t']?> fois</strong>. Si l'on regarde de plus pret ca nous fait une moyenne de <strong><?= $stats['nb_t_par_mois']?> tirages</strong> par mois, avec environ <strong><?= ceil($stats['nbf_par_mois'])?> feuilles</strong> en moyenne. Vous tirez<strong> <?= $stats['nb_moy_par_mois']?> copies </strong> par tirage.Je ne vous epargne pas le chiffre d'affaire : <?= round($stats['ca'])?>euros depuis le début, si l'on enleve les <strong><font style="color:red;"><?=round($stats['doit'])?> euros</font></strong> que l'on nous doit :<?= round($stats['ca2']) ?> euros. Vous nous avez donné <?= round($stats['ca1']) ?> euros. Nous sommes donc <?= $stats['benf'] ?>€, mais c'est sans compter le prix du loyer des condos qui à raison de 50 euros par mois nous ferai...  ! Le big data est là, je vous laisse regarder :)<br /> ps : et c'est aussi <strong>1800 lignes</strong> de code !</div>
            

            <div class="col-md-6"><h3 class="well well-sm"><center> Statistiques par mois A3</center></h3><table class="table"><thead><tr><th>date</th><th>feuilles </th><th>tirages</th><th>moyenne</th><th>Prix</th><th>Prix payé</th><th>difference</th></tr></thead><tbody>

      <?php

      for($i = $stat['a3']['ii'];$i < $stat['a3']['fin']  ; $i++ )
      { 
          if($stat['a3']['stat'][$i] ['benef']>=0)  { $class = "success";}else{$class = "danger";}
          $date = date('m.y',$stat['a3']['stat'][$i]['ago']); ?>
         <tr class="<?= $class ?>">
                  <td><?=$date?></td>
                  
                  <td rel="tooltip" title="nombre de feuilles"><?=ceil($stat['a3']['stat'][$i]['nbf'])?></td>
                  <td rel="tooltip" title="nombre de tirages"><?=$stat['a3']['stat'][$i]['nbt']?></td>
                  <td rel="tooltip" title="nombre de feuilles par tirage"><?=ceil($stat['a3']['stat'][$i]['moy'])?></td>
                  <td rel="tooltip" title="Prix coutant"><?=ceil($stat['a3']['stat'][$i]['prix'])?>€</td>
                  <td rel="tooltip" title="Combien l'utilisateur a payé"><?=ceil($stat['a3']['stat'][$i]['prixpaye'])?>€</td>
                  <td rel="tooltip" title="Gain pour ce mois"><?=ceil($stat['a3']['stat'][$i]['benef'])?>€</td>
               </tr>
      <?php
      }
      $iii = 1; ?>
        <ul class="pagination">
          <?php while($iii < $stat['a3']['nb_page'] ) {?>
          <li><a href="?accueil&page=<?=$iii?>#stats"><?= $iii ?></a></li>
          <?php $iii++;}?>
        </ul>
        </tbody>
      </table>
      <?php $iii = 1;?>
      <ul class="pagination">
      <?php while($iii < $stat['a3']['nb_page']) { ?>
        <li><a href="?accueil&page=<?= $iii?>#stats"><?= $iii?></a></li>
      <?php $iii++;} ?>
      </ul>
    </div><div class="col-md-6"><h3 class="well well-sm"><center> Statistiques par mois A4</center></h3><table class="table"><thead><tr><th>date</th><th>feuilles </th><th>tirages</th><th>moyenne</th><th>Prix</th><th>Prix payé</th><th>difference</th></tr></thead><tbody>
  <?php
  for($i = $stat['a4']['ii'];$i < $stat['a4']['fin']  ; $i++ )
{ 
    if($stat['a4']['stat'][$i] ['benef']>=0)  { $class = "success";}else{$class = "danger";}
    $date = date('m.y',$stat['a4']['stat'][$i]['ago']); ?>
   <tr class="<?= $class ?>">
            <td><?=$date?></td>
            
            <td rel="tooltip" title="nombre de feuilles"><?=ceil($stat['a4']['stat'][$i]['nbf'])?></td>
            <td rel="tooltip" title="nombre de tirages"><?=$stat['a4']['stat'][$i]['nbt']?></td>
            <td rel="tooltip" title="nombre de feuilles par tirage"><?=ceil($stat['a4']['stat'][$i]['moy'])?></td>
            <td rel="tooltip" title="Prix coutant"><?=ceil($stat['a4']['stat'][$i]['prix'])?>€</td>
            <td rel="tooltip" title="Combien l\'utilisateur a payé"><?=ceil($stat['a4']['stat'][$i]['prixpaye'])?>€</td>
            <td rel="tooltip" title="Gain pour ce mois"><?=ceil($stat['a4']['stat'][$i]['benef'])?>€</td>
         </tr>
<?php
}

$iii = 1; ?>
        <ul class="pagination">
          <?php while($iii < $stat['a4']['nb_page'] ) {?>
          <li><a href="?accueil&pagea4=<?=$iii?>#stats"><?= $iii ?></a></li>
          <?php $iii++;}?>
        </ul>
        </tbody>
      </table>
      <?php $iii = 1;?>
      <ul class="pagination">
      <?php while($iii < $stat['a4']['nb_page']) { ?>
        <li><a href="?accueil&pagea4=<?= $iii?>#stats"><?= $iii?></a></li>
      <?php $iii++;} ?>
      </ul>
