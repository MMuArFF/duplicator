<?php


if(isset($_POST['nb_f']))
{ ?>
     
 <div class="alert alert-info">
  <strong>Info!</strong>Merci d'avoir indiqué ce changement, vous pouvez fermer cette page ou revenir à l'<a href="index.php">Accueil</a>
</div>';
    <?php
}

?>

    <form class="form-horizontal" action="" method="post">
<fieldset>
<legend>Changement d'un des consommables</legend>
<div class="form-group">
  <label class="col-md-4 control-label" for="machine">Quelle machine ?</label>  
  <div class="col-md-4">
  
    <select id="machine" name="machine" class="form-control">
      <option value="A4">Duplicopieur A4</option>
      <option value="A3">Duplicopieur A3</option>
  
    </select>
   

  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="nb_f">Nombre de Passages</label>  
  <div class="col-md-4">
  <input id="nb_f" name="nb_f"  class="form-control input-md" required type="text">
  <span class="help-block"></span>  
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="nb_m">Nombre de master</label>  
  <div class="col-md-4">
  <input id="nb_m" name="nb_m"  class="form-control input-md" required type="text">
  <span class="help-block"></span>  
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="rv">Encre ou master</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="rv-0">
      <input name="eoum" id="rv-0" value="encre" type="radio">
     Encre
    </label>
     <label for="rv-1">
      <input name="eoum" id="rv-1" value="master" type="radio">
      Master
    </label>
	</div>
  </div>
</div>
   
   <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12"><button id="singlebutton" name="singlebutton" class="btn btn-success btn-block">enregistrer !</button></div>
        </div>
      </div>
    </div>
   </fieldset></form>

