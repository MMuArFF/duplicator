<?php
include('conf.php');
function template(string $file, array $variables = []): string
{
    extract($variables, EXTR_SKIP);
    ob_start();
    include($file);
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
}
function print_array($array) {

    print("<pre>".print_r($array,true)."</pre>");
}
Class Pdotest
{
    private function pdo_connect()
    {
    	include('conf.php');
        try {
            $db = new PDO($conf['dsn'], $conf['login'], $conf['pass']);
        }
        catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
        return $db;
    }
    function last_news()
    {
        $con   = New Pdotest;
        $db    = $con->pdo_connect();
        $query = $db->query('SELECT * FROM news order by id DESC limit 0,3');
        $i     = 0;
        while ($result = $query->fetch(PDO::FETCH_OBJ)) {
            $array[$i] = array(
                'time' => strftime("%A %d %B %Y", $result->time),
                'titre' => $result->titre,
                'news' => $result->news
            );
            $i++;
        }
        return $array;
    }
    function insert_email($email)
    {
        $con   = New Pdotest;
        $db    = $con->pdo_connect();
        $query = $db->prepare('INSERT into email VALUES("",:email)');
        $query->bindParam(':email', $email);
        $query->execute() or die('<div class="alert alert-danger">
  		<strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false">
		</div>');
        $result = '<div class="alert alert-success">
 		 <strong>Succes!</strong> votre email abien été ajouté
		</div>';
        return $result;
    }
    function nombre_feuilles_depuis($machine, $now, $ago)
    {
        $con       = New Pdotest;
        $db        = $con->pdo_connect();
        $sql       = "";
        $nbf_total = array();
        if (!empty($now)) {
            $nbf_total['ago'] = $now - $ago;
            $sql              = ' WHERE date < ' . $now . ' AND date >' . $nbf_total['ago'] . '';
        }
        $query                 = $db->query('select sum(rv) as nbr from ' . $machine . ' ' . $sql . '');
        $result                = $query->fetch(PDO::FETCH_OBJ);
        $nbf_total['nbf']      = $result->nbr;
        $query                 = $db->query('select sum(prix) as nbr from ' . $machine . ' ' . $sql . '');
        $result                = $query->fetch(PDO::FETCH_OBJ);
        $nbf_total['prix']     = $result->nbr;
        $query                 = $db->query('select sum(cb) as nbr from ' . $machine . ' ' . $sql . '');
        $result                = $query->fetch(PDO::FETCH_OBJ);
        $nbf_total['prixpaye'] = $result->nbr;
        $query                 = $db->query('select count(*) as nbr from ' . $machine . '' . $sql . '');
        $result                = $query->fetch(PDO::FETCH_OBJ);
        $nbf_total['nbt']      = $result->nbr;
        if (!empty($nbf_total['nbt'])) {
            $nbf_total['moy'] = $nbf_total['nbf'] / $nbf_total['nbt'];
        } else {
            $nbf_total['moy'] = $nbf_total['nbf'];
        }
        $nbf_total['benef'] = $nbf_total['prixpaye'] - $nbf_total['prix'];
        
        return $nbf_total;
    }
    
    function stats_par_mois($machine)
    {
        $con    = New Pdotest;
        $db     = $con->pdo_connect();
        $query  = $db->query('SELECT date from ' . $machine . ' order by id asc limit 1');
        $result = $query->fetch(PDO::FETCH_OBJ);
        $now    = time();
        $i      = 0;
        $mois   = 86400 * 30;
        while ($now >= $result->date) {
            $stat[$i] = $con->nombre_feuilles_depuis($machine, $now, $mois);
            $now      = $stat[$i]['ago'];
            $i++;
        }
        $stats_par_mois['nb_f'] = 0;
        $stats_par_mois['nb_t'] = 0;
        $stats_par_mois['nb_moy_par_mois'] = 0;
        for ($i = 0; $i < count($stat); $i++) {
            $stats_par_mois['nb_f']  += $stat[$i]['nbf'] ;
            $stats_par_mois['nb_t']  += $stat[$i]['nbt'] ;
            $stats_par_mois['nb_moy_par_mois'] += $stat[$i]['moy'] ;
        }
        $stats_par_mois['fin'] = count($stat);
        
        return $stats_par_mois;
    }
    
    
    function blablastats($fin=0,$nb_t=0,$nb_f=0,$nb_moy_par_mois=0,$ca_voulu = 0,$ca_cb_paye =0,$ca_declare_paye =0)
    {
        $con             = New Pdotest();
        $db              = $con->pdo_connect();
        $machines = array('A4','A3');
        foreach ($machines as $machine){
            $stats_par_mois[$machine]  = $con->stats_par_mois($machine);
            foreach($stats_par_mois as $mois){
                $fin += $mois['fin'];
                $nb_t += $mois['nb_t'];
                $nb_f += $mois['nb_f'];
                $nb_moy_par_mois += $mois['nb_moy_par_mois'];
            }
            $query= $db->query('select * FROM '.$machine.'');
            while($result  = $query->fetch(PDO::FETCH_OBJ))
            {
                $ca_voulu += (!is_numeric($result->prix)) ? 0: $result->prix;
                $ca_cb_paye += (!is_numeric($result->cb)) ? 0: $result->cb; 
                if($result->paye == "oui"){ $ca_declare_paye += $result->prix; }
            }      
        }
        $doit =  $ca_voulu - $ca_declare_paye;
        $benf = $ca_cb_paye - $ca_voulu;
        $nb_t_par_mois   = round(($nb_t / $fin),2);
        $nbf_par_mois    = round(($nb_f / $fin),2);
        $nb_moy_par_mois = round($nb_moy_par_mois / $fin,2);
        $stats = array(
            'nb_f' => $nb_f,
            'nb_t' => $nb_t,
            'nb_t_par_mois' => $nb_t_par_mois,
            'nbf_par_mois' => $nbf_par_mois,
            'nb_moy_par_mois' => $nb_moy_par_mois,
            'ca' => $ca_voulu,
            'ca2' => $ca_declare_paye,
            'ca1' => $ca_cb_paye,
            'benf' => $benf,
            'doit' => $doit
        );
        return $stats;
    }
    
    function select_first_entry($machine)
    {
        $con    = New Pdotest;
        $db     = $con->pdo_connect();
        $query  = $db->query('SELECT date from ' . $machine . ' order by id asc limit 1');
        $result = $query->fetch(PDO::FETCH_OBJ);
        return $result;
    }
    
    function stats_by_machine($machine, $page)
    {
        $con   = New Pdotest;
        $now   = time();
        $i     = 0;
        $stat  = array();
        $first = $con->select_first_entry($machine);
        $first = $first->date;
        $mois  = 86400 * 30;
        while ($now >= $first) {
            $stat[$i] = $con->nombre_feuilles_depuis($machine, $now, $mois);
            $now      = $stat[$i]['ago'];
            $i++;
        }
        $fin     = count($stat);
        $nb_page = round($fin / 12);
        $reste   = fmod($fin, 12);
        $fin     = 12 * $page;
        $ii      = $fin - 12;
        if ($page == $nb_page + 1) {
            $reste = 12 - $reste;
            $fin   = $fin - $reste;
        }
        $stats_by_machine = array(
            'stat' => $stat,
            'fin' => $fin,
            'reste' => $reste,
            'ii' => $ii,
            'nb_page' => $nb_page
        );
        return $stats_by_machine;
    }
    function get_price()
    {
        $con              = New Pdotest;
        $db               = $con->pdo_connect();
        $query            = $db->query('select * from prix');
        while($result           = $query->fetch(PDO::FETCH_OBJ))
        {
        		$prix[$result->machine][$result->type]['unite'] = $result->unite;
        		$prix[$result->machine][$result->type]['pack'] = $result->pack;
        }
        $query = $db->query('select * from papier');
        $result           = $query->fetch(PDO::FETCH_OBJ);
		$prix['papier']['A3'] = $result->prix*2;
		$prix['papier']['A4'] = $result->prix;
        return $prix;
    }
    function insert_prix($machine,$type,$prix_pack,$prix_unite)
	{
	  $con = new Pdotest;
	  $db = $con->pdo_connect();
	  $query = $db->prepare('UPDATE prix SET pack = :prix_pack , unite = :prix_unite  WHERE machine = :machine AND type = :type');
	  $query->bindparam(':machine' ,$machine);
	  $query->bindparam(':prix_pack' , $prix_pack);
	  $query->bindparam(':prix_unite', $prix_unite);
	  $query->bindparam(':type', $type);
	  $query->execute() or die(print_r($query->errorInfo()));
	  
	  
	}
     function del_tirage($id,$machine){
          $con = New Pdotest;
          $db = $con->pdo_connect();
          $id = ceil($id);
          $machines = array("A3","A4","photocop");
          in_array($machine,$machines) or die('donttrytohackme');
          $db->query('DELETE from '.$machine.' WHERE id= '.$id.'');

         }
    function get_price_devis($machine, $nb_f, $nb_p, $nb_m)
    {		
        $con                 = New Pdotest;
        $price               = $con->get_price();
        if(($machine =="pA4") OR ($machine =="pA3"))
    	{ 
    		$taille = substr($machine,-2);
    		$price['devis']['machine'] = "photocop"; 
    		
    	}
        else{ 
       		$price['devis']['machine'] = $machine;
        	$taille = $machine;
    	}
        $price['devis']['f'] = $nb_f * $price['papier'][$taille];
        $price['papier'] = $price['papier'][$taille];
        $price['devis']['m'] = $nb_m * $price[$taille]['master']['unite'];
        $price['devis']['p'] = $nb_p * $price[$taille]['encre']['unite'];
        $price['devis']['t'] = $price['devis']['p'] + $price['devis']['m'] + $price['devis']['f'];
        if($price['devis']['machine'] =="photocop")
        {
        	 ($taille =="A3") ? $unite =  $price['photocop']['encre']['unite'] : $unite =  $price['photocop']['encre']['unite']/2;
        	  $price['photocop']['encre']['unite']  = $unite;
        	  $price['devis']['p'] = $nb_p *$unite;
        	  $price['devis']['t'] = $price['devis']['f'] +  $price['devis']['p'];
        }
   			
        return $price;
    }
    function get_price_photocop($papier, $nbp,$nbf)
    {
        $con   = New Pdotest;
        $price = $con->get_price();
        !($papier =="A3")? $price['unitaire'] =$price['photocop']['encre']['unite'] /2 : $price['unitaire'] = $price['photocop']['encre']['unite'] ;
        $price['prix_papier'] = $price['papier'][$papier] * $nbf;
        $price['prix_passage'] = $price['unitaire'] * $nbp;
        $price['total'] = ($price['papier'][$papier] * $nbf )+ ($price['unitaire'] * $nbp);
        return $price;
    }
    function get_last_number($machine)
    {
        $con                = new Pdotest;
        $db                 = $con->pdo_connect();
        $query              = $db->query('SELECT * FROM ' . $machine . ' ORDER by id DESC limit 1');
        $result             = $query->fetch(PDO::FETCH_OBJ);
        $last['master_av']  = ceil($result->master_ap);
        $last['passage_av'] = ceil($result->passage_ap);
        return $last;
    }
    
    function pdo_insert($machine, $type, $contact, $master_av, $master_ap, $passage_av, $passage_ap, $rv, $prix, $paye, $cb, $mot, $date)
    {
        $con   = new Pdotest;
        $db    = $con->pdo_connect();
        $query = $db->prepare('INSERT into ' . $machine . ' VALUES ("",:type,:contact,:master_av,:master_ap,:passage_av,:passage_ap,:rv,:prix,:paye,:cb,:mot,:date)');
        $query->bindParam(':type', $type);
        $query->bindParam(':contact', $contact);
        $query->bindParam(':master_av', $master_av);
        $query->bindParam(':master_ap', $master_ap);
        $query->bindParam(':passage_av', $passage_av);
        $query->bindParam(':passage_ap', $passage_ap);
        $query->bindParam(':rv', $rv);
        $query->bindParam(':prix', $prix);
        $query->bindParam(':paye', $paye);
        $query->bindParam(':cb', $cb);
        $query->bindParam(':mot', $mot);
        $query->bindParam(':date', $date);
        
        $query->execute() or die(print_r($query->errorInfo()));
        
    }
    function insert_photocop($type, $contact, $nb_f, $rv, $prix, $paye, $cb, $mot, $date)
    {
        $con   = new Pdotest;
        $db    = $con->pdo_connect();
        $query = $db->prepare('INSERT into photocop VALUES ("",:type,:contact,:nb_f,:rv,:prix,:paye,:cb,:mot,:date)');
        $query->bindParam(':type', $type);
        $query->bindParam(':contact', $contact);
        $query->bindParam(':nb_f', $nb_f);
        $query->bindParam(':rv', $rv);
        $query->bindParam(':prix', $paye);
        $query->bindParam(':paye', $prix);
        $query->bindParam(':cb', $cb);
        $query->bindParam(':mot', $mot);
        $query->bindParam(':date', $date);
        $query->execute() or die('<div class="alert alert-danger"> <strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false"></div>');
    }
    function insert_cons($date, $machine, $type, $nb_p, $nb_m)
    {
        $con   = new Pdotest;
        $db    = $con->pdo_connect();
        $query = $db->prepare('INSERT into cons VALUES ("",:date,:machine,:type,:nb_p,:nb_m)');
        $query->bindparam(':date', $date);
        $query->bindparam(':machine', $machine);
        $query->bindparam(':nb_m', $nb_m);
        $query->bindparam(':nb_p', $nb_p);
        $query->bindparam(':type', $type);
        $query->execute() or die(print_r($query->errorInfo()));
    }
    function delete_mail()
    {
        $con = new Pdotest;
        $db  = $con->pdo_connect();
        $db->query('DELETE From email') or die('<div class="alert alert-danger"><strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false"></div>');
    }
    function count_emails()
    {
        $emails =array();
        $con    = new Pdotest;
        $db     = $con->pdo_connect();
        $query  = $db->query('SELECT count(*) as nbr from email');
        $result = $query->fetch(PDO::FETCH_OBJ);
        if (!empty($result->nbr)) {
            $query  = $db->query('SELECT * from email');
            while($result1 = $query->fetch(PDO::FETCH_OBJ))
                {
                    $emails[] = $result1->email;
                }
        }
        return $emails;
    }
    function get_mots()
    {
          $con = new Pdotest;
          $db = $con->pdo_connect();
          $query = $db->query('SELECT * FROM photocop WHERE mot != "" order by id DESC');
          $i = 0;
          if(empty($query)){ goto two;}
          while($result =$query->fetch(PDO::FETCH_OBJ))
          {

             $timest =  $result->date;
             $mots['photocop'][$i]['date'] = date('d.m.y',$timest);
             $mots['photocop'][$i]['mot'] = $result->mot;
             $mots['photocop'][$i]['id'] = $result->id;
             if(strlen($result->contact)> 13) { $mots['photocop'][$i]['contact']= substr($result->contact, 0, 10).'...'; }
             else { $mots['photocop'][$i]['contact'] = $result->contact;}
             $i++;
          }
          two:
          $query = $db->query('SELECT * FROM A4 WHERE mot != "" order by id DESC');
          $i = 0;
          if(empty($query)){ goto three;}
          while($result =$query->fetch(PDO::FETCH_OBJ))
          {
             $timest =  $result->date;
             $mots['A4'][$i]['date'] = date('d.m.y',$timest);
             $mots['A4'][$i]['mot'] = $result->mot;
             $mots['A4'][$i]['id'] = $result->id;
             if(strlen($result->contact)> 13) { $mots['A4'][$i]['contact']= substr($result->contact, 0, 10).'...'; }
             else { $mots['A4'][$i]['contact'] = $result->contact;}
             $i++;
          }
       		three:
        	$query = $db->query('SELECT * FROM A3 WHERE mot != "" order by id DESC');
       		$i=0;
       		if(empty($query)){ goto four;}
            while($result =$query->fetch(PDO::FETCH_OBJ))
          {
              $timest =  $result->date;
             $mots['A3'][$i]['date'] = date('d.m.y',$timest);
             $mots['A3'][$i]['mot'] = $result->mot;
             $mots['A3'][$i]['id'] = $result->id;
             if(strlen($result->contact)> 13) { $mots['A3'][$i]['contact']= substr($result->contact, 0, 10).'...'; }
             else { $mots['A3'][$i]['contact'] = $result->contact;}
             $i++;
          }
          four:
       	return $mots;
        
    }
     function get_machines()
          {
            $machines = array('A4','A3','photocop');
            return $machines;
          }
    function prix_du($machine)
    {
    	$con = new Pdotest;
        $db = $con->pdo_connect();
        $query = $db->query('SELECT sum(prix) AS nbr FROM '.$machine.' WHERE paye = "non"');
        $result = $query->fetch(PDO::FETCH_OBJ);
        $euros = $result->nbr;
        return $euros;
    }
    function last($machine,$sql)
    {
    	if($machine == "photocop") {}
	    $con = new Pdotest;
	    $db = $con->pdo_connect();
	    $query = $db->query('SELECT * FROM '.$machine.' '.$sql.'') ;
	    $i = 0;
	    while($result = $query->fetch(PDO::FETCH_OBJ))
	    {
	      $last[$i]['date'] = date('d.m.y',$result->date);
	      $last[$i]['contact'] = substr($result->contact, 0, 10).'...';
	      $last[$i]['prix'] = round($result->prix,2);
	      $last[$i]['id'] = $result->id;
	      $i++;
	    }
	    return $last ;
     }
     function update_news($titre,$texte,$id)
     {
     	$con = new Pdotest;
	    $db = $con->pdo_connect();
	    $query = $db->prepare('UPDATE news SET titre = :titre, news =:texte WHERE id ='.$_POST['id2'].' ');
        $query->bindParam(':titre', $titre);
        $query->bindParam(':texte', $texte);
        $query->execute() or die ('<div class="alert alert-danger"><strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false"></div>');
     }
    
     function insert_news($titre,$texte)
	  {
	   $con = new Pdotest;
	   $db = $con->pdo_connect();
	   $temps = time();
	   $query = $db->prepare('INSERT into news VALUES ("",:temps,:titre,:texte)');
	   $query->bindParam(':temps', $temps);
	   $query->bindParam(':titre', $titre);
	   $query->bindParam(':texte', $texte);
	   $query->execute() or die ('<div class="alert alert-danger"> <strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false"></div>');
	  }
	   function delete_news($id){
	   	$con = new Pdotest;
	    $db = $con->pdo_connect();
          $db->query('DELETE from news where id = '.$_POST['id'].'') or die ('<div class="alert alert-danger">  <strong>Danger!</strong> Une erreur s\'est produite.<a href="javascript:" onclick="history.go(-1); return false"></div>');
        }
        function get_news($id)
        {
        	$con = new Pdotest;
	   		$db = $con->pdo_connect();
            $id = ceil($id);
        	if(!empty($id))
        	{
        		$query = $db->query('SELECT * from news WHERE id = '.$id.'');
        		$result = $query->fetch(PDO::FETCH_OBJ);
        		$array['titre'] = $result->titre;
          		$array['temps'] = date('d.m.y',$result->time);
          		$array['news'] = $result->news;
          		$array['id'] = $result->id;
          	
          	}
          	else
          	{
          		$query = $db->query('SELECT * from news ');
          		$i = 0 ;
          		while($result = $query->fetch(PDO::FETCH_OBJ)){
          			$array[$i]['titre'] = $result->titre;
          			$array[$i]['temps'] = date('d.m.y',$result->time);
          			$array[$i]['news'] = $result->news;
          			$array[$i]['id'] = $result->id;
          	
          			$i++;
          		}
          		$result = $array;
          	}
          	return $result;
        }
	function insert_cons_photocop()
	{
		  
    	$con= new Pdotest;
    	$db = $con->pdo_connect();
    	$cons = $con->get_cons('photocop');
    	$date = time();	
    	$query = $db->prepare('INSERT into cons VALUES ("",:date,"photocop","",:nb_p,"0")');
        $query->bindparam(':date',$date);
        $query->bindparam(':nb_p',$cons['photocop']['nb_debut']); 
    	$query->execute() or die (print_r($query->errorInfo()));
    }
        
function get_cons($machine)
{

      $con = new Pdotest;
      $db = $con->pdo_connect();
      $prix = $con->get_price();
      if($machine != 'photocop')
      {
      	$nb = $con->get_last_number($machine);
      }
      $query =$db->query('SELECT * FROM cons where machine = "'.$machine.'"');
      $i=0;
      while ($result = $query->fetch(PDO::FETCH_OBJ))
      {
        $res[$i]['date'] = $result->date;
        $res[$i]['type'] = $result->type;
        $res[$i]['nb_p'] = $result->nb_p;
        $res[$i]['nb_m'] = $result->nb_m;
        $i++;
      }
      $max = count($res) ;
      for($i=0; $i < $max  ;$i++)
      {
        if($machine =='photocop')
        {
        	$res['photocop'][$i]['temps'] =  $res[$i]['date'];
            $res['photocop'][$i]['nb_m'] = $res[$i]['nb_m'];
        	if($i > 0 )
          	{
          		$ii = $i -1; 
          		$res['temps_moy'][$i] =  $res[$i]['date'] - $res[$ii]['date'] ; 
          		$res['nb_f'][$i] = $res[$i]['nb_p'] - $res[$ii]['nb_p'];
          		$ii++;	
      		}
        }
        else
        { 
          if($res[$i]['type'] == "master")
          {
            if(!isset($i_master)){ $i_master = 0;}
            $res['master'][$i_master]['temps'] =  $res[$i]['date'];
            $res['master'][$i_master]['nb_m'] = $res[$i]['nb_m'];
            if( $i_master >0 )
            { 
            	$ii_master = $i_master -1; 
            	$res['master']['temps_moy'][$i_master] =  $res['master'][$i_master]['temps'] - $res['master'][$ii_master]['temps']; 
            	$res['master']['nb_m_moy'][$i_master] = $res['master'][$i_master]['nb_m'] - $res['master'][$ii_master]['nb_m'];
            	$ii_master++;
            }
            $i_master++; 

          }
          if($res[$i]['type'] == "encre")
          {
            if(!isset($i_encre)){$i_encre = 0;}
            $res['encre'][$i_encre]['temps'] =  $res[$i]['date'];
            $res['encre'][$i_encre]['nb_p'] = $res[$i]['nb_p'];
            if( $i_encre >0 )
            { 
            	$ii_encre = $i_encre -1;
            	$res['encre']['temps_moy'][$i_encre] =  $res['encre'][$i_encre]['temps']- $res['encre'][$ii_encre]['temps']; 
            	$res['encre']['nb_p_moy'][$i_encre] =  $res['encre'][$i_encre]['nb_p']- $res['encre'][$ii_encre]['nb_p']; 
            	$ii_encre++;
            }
            $i_encre++; 
          }

        }
      }
      if($machine =='photocop')
      { 
      	$res['photocop']['moyenne_total']['temps'] = array_sum($res['temps_moy'])/count($res['temps_moy']);
      	$res['photocop']['moyenne_total']['nb_p'] = array_sum($res['nb_f'])/count($res['nb_f']);
      	$query = $db->query('SELECT sum(nb_f) as nbr from photocop WHERE date > '.$res[$ii]['date'].' ');
      	$result = $query->fetch(PDO::FETCH_OBJ);
      	$res['photocop']['nb_actuel'] = $result->nbr;
      	$query = $db->query('SELECT sum(nb_f) as nbr from photocop  ');
      	$result = $query->fetch(PDO::FETCH_OBJ);
      	$res['photocop']['nb_debut'] = $result->nbr;
       	$res['photocop']['temps_depuis'] = time() -  $res[$ii]['date'];
       	$res['photocop']['temps_jusqua'] = $res['photocop']['moyenne_total']['temps'] - $res['photocop']['temps_depuis'];
      	$res['photocop']['prix_calcule'] = $prix['photocop']['encre']['pack'] /$res['photocop']['moyenne_total']['nb_p'];
      	if($res['photocop']['temps_jusqua']  < -30){ $$res['photocop']['class'] = "danger" ;}
		if(($res['photocop']['temps_jusqua']  < 0) AND ($res['photocop']['temps_jusqua']  > -30)){$res['photocop']['class'] = "warning";}
		if(($res['photocop']['temps_jusqua']  > 0)&&($res['photocop']['temps_jusqua']  < 30)){$res['photocop']['class'] = "info" ;}
		if($res['photocop']['temps_jusqua']  > 30){$res['photocop']['class'] = "success";}
		($res['photocop']['prix_calcule']  > $prix['photocop']['encre']['unite'])? $res['photocop']['color'] = "green":$res['photocop']['color'] = "red";
  	  }
      else
      {
        $res['encre']['moyenne_totale']['temps'] = array_sum($res['encre']['temps_moy'])/count($res['encre']['temps_moy']);
        $res['master']['moyenne_totale']['temps'] = array_sum($res['master']['temps_moy'])/count($res['master']['temps_moy']);
        $res['master']['moyenne_totale']['nb_m'] = array_sum($res['master']['nb_m_moy'])/count($res['master']['nb_m_moy']);
        $res['encre']['moyenne_totale']['nb_p'] = array_sum($res['encre']['nb_p_moy'])/count($res['encre']['nb_p_moy']);
        $res['master']['nb_actuel'] = $nb['master_av'] - $res['master'][$ii_master]['nb_m'];
        $res['encre']['nb_actuel'] = $nb['passage_av'] - $res['encre'][$ii_encre]['nb_p'];
        $res['master']['temps_depuis'] = time() -  $res['master'][$ii_master]['temps'];
        $res['encre']['temps_depuis'] =time()- $res['encre'][$ii_encre]['temps'];
        $res['encre']['temps_jusqua'] = $res['encre']['moyenne_totale']['temps'] - $res['encre']['temps_depuis'];
        $res['master']['temps_jusqua'] = $res['master']['moyenne_totale']['temps'] - $res['master']['temps_depuis'];
        $res['master']['prix_calcule'] = $prix[$machine]['master']['pack'] / $res['master']['moyenne_totale']['nb_m'] ;
        $res['encre']['prix_calcule'] = $prix[$machine]['encre']['pack'] / $res['encre']['moyenne_totale']['nb_p'] ;
        ($res['encre']['prix_calcule']< $prix[$machine]['encre']['unite']) ? $res['encre']['color'] = "green": $res['encre']['color'] = "red";
        
        ($res['master']['prix_calcule']< $prix[$machine]['master']['unite']) ? $res['master']['color'] = "green": $res['master']['color'] = "red";
        if(($res['encre']['temps_jusqua']/86400) < -30){ $res['encre']['class'] = "danger" ;}
		if((($res['encre']['temps_jusqua']/86400) < 0) AND ($res['encre']['temps_jusqua'] > -30)){ $res['encre']['class'] = "alert";}
		if((($res['encre']['temps_jusqua']/86400) > 0)&&($res['encre']['temps_jusqua'] < 30)){ $res['encre']['class'] = "info" ;}
		if(($res['encre']['temps_jusqua']/86400) > 30){ $res['encre']['class'] = "success";}
		if($res['master']['temps_jusqua'] < -30){ $res['master']['class'] = "danger" ;}
		if(($res['master']['temps_jusqua'] < 0) AND ($res['master']['temps_jusqua'] > -30)){$res['master']['class'] = "warning";}
		if(($res['master']['temps_jusqua'] > 0)&&($res['master']['temps_jusqua'] < 30)){$res['master']['class'] = "info" ;}
		if($res['master']['temps_jusqua'] > 30){$res['master']['class'] = "success";}

      }

	
	/**/

      
    return $res;
}

    function insert_papier($papier){
          $con = new Pdotest;
          $db = $con->pdo_connect();
          $query = $db->prepare('UPDATE papier set prix = :papier WHERE id = 0');
          $query->bindparam(':papier',$papier);
          $query->execute() or die();

        }
    function get_tirage($id,$machine)
    {
        $con = new Pdotest;
        $db = $con->pdo_connect();
        $machines = array("A3","A4","photocop");
        in_array($machine,$machines) or die('donttrytohackme');
        $id = ceil($id);
        $query = $db->query('SELECT * FROM '.$machine.' WHERE id = '.$id.' ');
        $res = $query->fetch(PDO::FETCH_ASSOC);
        $res['machine'] = $machine;
        return $res;
    }
    function update_tirage($id,$form,$machine){
        $con = new Pdotest;
        $old = $con->get_tirage($id,$machine);
        $old['save']= "";
        $update = array_diff($form, $old);
         $id = ceil($id);
        $machines = array("A3","A4","photocop");
        in_array($machine,$machines) or die('donttrytohackme');
        $sql = 'UPDATE '.$machine.' SET' ;
        foreach ($update as $key => $column) {
            if($key != 'save'){
                 $sql = $sql.' '.$key.' = :'.$key.' , ';
                }
        }
        $sql = substr($sql, 0, -2).' WHERE id = '.$id;
       
        $db = $con->pdo_connect();
        $query = $db->prepare($sql);
        foreach ($update as $key => $column) {
            if($key != 'save'){
                $query->bindparam(':'.$key,$column);
                //echo '$query->bindparam(:'.$key.','.$column.') <br/>';
            }
        }
        $query->execute() or die(print_r($query->errorInfo()));
       
    }
    function secure_post($POST){
        $key = array_keys($_POST);
        foreach ($key as $value){
        	
          $_POST[$value]= htmlentities($_POST[$value]);
        }
      }

        
}



