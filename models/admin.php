<?php
function Action()
{
  $con    = new Pdotest;  
  $array=array();
  if(!isset($_SESSION['user'])&& isset($_POST['password']))
  {
  		$hash = '$2y$10$QTm6Y6ysJCgWIn.NKO3iy.7boYx62lwYKI8uD4yzV0VYUETN7KpgW';
  		if (password_verify($_POST['password'], $hash)) {
      $_SESSION['user'] = "1";
      } else {
      die('Le mot de passe est invalide.');
      }
  }

  if(isset($_SESSION['user']))
  {
    if(isset($_GET['prix'])){
      if(isset($_POST)){$con->secure_post($_POST);}


      if(isset($_POST['prix_pack']))
      {
        $con->insert_prix($_POST['machine'],$_POST['type'],$_POST['prix_pack'],$_POST['prix_unite']);

      }
      if(isset($_POST['papier_A3']))
      {
       
        $con->insert_papier($_POST['papier_A3']/2);
        
      }
      $array['prix'] = $con->get_price();
      $array['cons']['A4'] = $con->get_cons('A4');
      $array['cons']['A3'] = $con->get_cons('A3');
      $array['cons']['photocop'] = $con->get_cons('photocop');
      $array['machines']['photocop'] = 1;
      $array['machines']['A4'][0] = 'encre';
      $array['machines']['A3'][0] = 'encre';
      $array['machines']['A4'][1] = 'master';
      $array['machines']['A3'][1] = 'master';
   }
    if(isset($_GET['news']))
    {
       if(isset($_POST['titre']))
      {
           $titre = htmlentities($_POST['titre']);
           $texte  = htmlentities($_POST['texte']);
           if(isset($_POST['id2']))
           {
                $id= ceil($_POST['id2']);
                $con->update_news($titre,$texte,$id);                
           }  
           else{ $con->insert_news($titre,$texte);}

       }
       elseif(isset($_POST['id']))
       {
          $id = ceil($_POST['id']);
          if(isset($_POST['singlebutton']))
          {          
           $con->delete_news($id) ;
          }
          else
          {
            $array['new_edit'] = $con->get_news($id);
          }
        }
        else{
          $array['news'] = $con->get_news("");
        }
    }
    if(isset($_GET['edit']))
    {

       $machine = $_GET['table'];
       $id = $_GET['edit'];
       if(isset($_POST['delete']))
        {
         $con->del_tirage($id,$machine);  
        }
       if(isset($_POST['save']))
        {
         $con->update_tirage($id,$_POST,$machine);         
        }
        $array['tirage'] = $con->get_tirage($id,$machine);
    }
    if(isset($_GET['tirages']))
    {
          if(!isset($_GET['order']) && !isset($_GET['paye'])){ $phrasze = 'Voir seulement les <a href="?admin&tirages&paye">nons-payés</a> ou les classer par <a href="?admin&tirages&order">ordre de prix</a>'; $sql ="ORDER By id DESC";}
          if(!isset($_GET['order']) && isset($_GET['paye'])) {  $phrasze = 'Voir tous les <a href=".?admin&tirages">derniers tirages</a> ou classer les nons payés par <a href="?admin&tirages&paye&order">ordre de prix</a>'; $sql =' WHERE paye = "non" ORDER By id DESC' ; }
          if(isset($_GET['order']) && !isset($_GET['paye'])){ $phrasze = 'Voir seulement les <a href="?admin&tirages&paye">nons-payés</a>'; $sql = ' ORDER by prix  * 1 DESC'; } 
          if(isset($_GET['order']) && isset($_GET['paye'])){$phrasze ='Voir tous les <a href="?admin&tirages">derniers tirages</a>';  $sql = ' WHERE paye = "non" ORDER by prix  * 1 DESC';}
          $array['phrase'] = $phrasze;
          $array['machines'] = $con->get_machines();
          foreach ($array['machines']as $key)
          {
            $array['last'][$key] = $con->last($key,$sql);
            $array['prix_du'][$key] = $con->prix_du($key);
            
          }
    }
    if(isset($_GET['mots']))
    {
         $array['mots'] = $con->get_mots();
    }
    if(isset($_POST['delmail']))  {   $con->delete_mail();  }
    $array['emails'] = $con->count_emails();
            
  }
  return template("../view/admin.html.php",$array);
}
?>
 